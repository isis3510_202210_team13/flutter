import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wheelio_app/models/places.dart';

abstract class HomeMapEvents {}

class OnMyLocationUpdate extends HomeMapEvents {
  final LatLng location;

  OnMyLocationUpdate(
    this.location,
  );
}

class OnMapTap extends HomeMapEvents {
  final LatLng location;

  OnMapTap(
    this.location,
  );
}

class OnGPSEnabled extends HomeMapEvents {
  final bool enabled;

  OnGPSEnabled(this.enabled);
}

class GoToPlace extends HomeMapEvents {
  final Place place;

  GoToPlace(this.place);
}

class ConfirmPoint extends HomeMapEvents {
  final Place place;
  final bool isArrival;

  ConfirmPoint(this.place, this.isArrival);
}
