part of 'authentication_bloc.dart';

abstract class AuthenticationState extends Equatable {
  const AuthenticationState();

  @override
  List<Object?> get props => [];
}

class AuthenticationInitial extends AuthenticationState {
      @override
  List<Object?> get props => [];
}

class AuthenticationSuccess extends AuthenticationState {
  final User? fbUser;
  const AuthenticationSuccess(User? streamUser, {this.fbUser});

}

class AuthenticationFailure extends AuthenticationState {
}