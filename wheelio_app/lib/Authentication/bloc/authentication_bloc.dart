import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:wheelio_app/Authentication/repository/authentication_repository.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthenticationRepository _authenticationRepository;
  final Stream<User?> _userStream = FirebaseAuth.instance.userChanges();

  AuthenticationBloc(this._authenticationRepository)
      : super(AuthenticationInitial()) {
    on<AuthenticationEvent>((event, emit) async {
      if (event is AuthenticationStarted) {
        try {
          await _authenticationRepository
              .performLogin("google.com", ["email openid"], {'locale': 'es'});
        } catch (e) {
          emit(AuthenticationFailure());
        }
      }

              await emit.forEach(_userStream,
            onData: (User? streamUser) {
              if(FirebaseAuth.instance.currentUser == null) {
                return AuthenticationFailure();
              }
              else return AuthenticationSuccess(FirebaseAuth.instance.currentUser);
            
            });
    });
  }
}
