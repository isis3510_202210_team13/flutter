import 'package:flutter/material.dart';
import 'package:wheelio_app/screens/driver_data.dart';

import '../../models/trip.dart';

class TripCard extends StatelessWidget {
  final Trip _trip;
  const TripCard(this._trip);
  final TextStyle ts = const TextStyle(fontSize: 20, color: Colors.black87);
  final TextStyle tst = const TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Card(
      child: ListTile(
        onTap: () {
          
          Navigator.of(context).push(
            
              MaterialPageRoute(builder: (context) => const DriverData(driverName: 'pepito',end: "xd",photo: "xd",plate:"xd",start: "xd",)));
        },
        title: Text('Destino: ' + _trip.otherAddressName.toString(), style: tst,),
        subtitle: Text('Salida:' + DateTime.fromMillisecondsSinceEpoch(_trip.tripDateTime).toString(), style: ts,),
        trailing: Text(_trip.pricePerSeat.toString() + " pesos",  style: ts,),
      ),
    ));
  }
}
