import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wheelio_app/Connectivity/bloc/connectivity_bloc.dart';
import 'package:wheelio_app/Connectivity/repository/connectivity_repository.dart';
import 'package:wheelio_app/screens/driver_data.dart';
import 'package:wheelio_app/screens/home_page_map.dart';
import 'package:wheelio_app/screens/loading_driver.dart';
import 'package:wheelio_app/screens/travel_confirmation_page.dart';
import 'package:wheelio_app/trip_active/bloc/trip_bloc.dart';
import 'package:wheelio_app/trip_search/bloc/trip_list_bloc.dart';
import 'package:wheelio_app/trip_search/ui/screens/travel_list.dart';

class TravelData extends StatefulWidget {
  const TravelData({Key? key}) : super(key: key);

  @override
  State<TravelData> createState() => _StatefulTravelData();
}

class _StatefulTravelData extends State<TravelData> {
  final locationController = TextEditingController();
  final priceController = TextEditingController();


  bool? _isDriver = false;
  bool? _goingHome = false;
  bool waiting = false;
  bool canceled = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(scaffoldBackgroundColor: const Color(0xFFFFE45C)),
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Ofrecer un viaje'),
            centerTitle: true, // appbar text center.
          ),
          body: MultiBlocListener(listeners: [
            BlocListener<ConnectivityBloc, ConnectivityState>(
                listener: (context, state) {
              if (context.read<ConnectivityBloc>().state is NoConnectivity) {
                context
                    .read<ConnectivityRepositoryImpl>()
                    .showBasicsFlash(context);
              } else if (context.read<ConnectivityBloc>().state is ConnectivityStatus) {
                context
                    .read<ConnectivityRepositoryImpl>()
                    .showConnectionRestablishedFlash(context);
              }
            }),
            BlocListener<TripListBloc, TripListState>(
                listener: (context, state) {
              if (context.read<ConnectivityBloc>().state is NoConnectivity) {
                context
                    .read<ConnectivityRepositoryImpl>()
                    .showBasicsFlash(context);
              }
            }),],
          child: BlocBuilder<TripListBloc, TripListState> (
              builder: (context, state) {
                _isDriver = context.read<TripBloc>().state.isDriver;
                var start = "";
                var end = "";
                var price = "";
                if(context.read<TripListBloc>().state is TripListInitial) {
                  context.read<TripListBloc>().add(TripPreferencesRetrived());
                }
                else if (context.read<TripListBloc>().state is TripPreferencesOn) {
                                    var preferences = (state as TripPreferencesOn).preferences;
                  if(preferences != null) {
                    
                  _goingHome = preferences[0].toLowerCase() == 'true';
                  price = preferences[1];
                  priceController.value = TextEditingValue(text: price);                 
                  start = preferences[2];
                  end = preferences[3];

                  if (end == 'Universidad de los andes') {
                    locationController.value = TextEditingValue(text: start);
                    
                  } else {
                    locationController.value = TextEditingValue(text: end);
                  }
                  }

                }
                return SingleChildScrollView(
                    child: Container(
                        margin: const EdgeInsets.symmetric(horizontal: 5.0),
                        child: Column(
                          children: [
                            const Divider(
                              color: Colors.black26,
                              indent: 10,
                              endIndent: 10,
                              height: 4,
                              thickness: 2,
                            ),
                            GridView.count(
                              shrinkWrap: true,
                              crossAxisCount: 2,
                              crossAxisSpacing: 2,
                              mainAxisSpacing: 4,
                              childAspectRatio: 3,
                              children: [
                                RadioListTile(
                                    title: const Text("Ir a la U"),
                                    value: false,
                                    groupValue: _goingHome,
                                    onChanged: (bool? value) {
                                      setState(() {
                                        _goingHome = value;
                                      });
                                    }),
                                RadioListTile(
                                    title: const Text("Ir a casa"),
                                    value: true,
                                    groupValue: _goingHome,
                                    onChanged: (bool? value) {
                                      setState(() {
                                        _goingHome = value;
                                      });
                                    })
                              ],
                            ),
                            const Text(
                              'Desde: ',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black54),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            if (_goingHome ?? true)
                              const Text('Universidad de los andes')
                            else
                              TextFormField(
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: 'Buscar',
                                ),
                                controller: locationController,

                              ),
                            const SizedBox(
                              height: 20,
                            ),
                            const Text(
                              'Hasta: ',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black54),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            if (_goingHome ?? false)
                              TextFormField(
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: 'Buscar',
                                ),
                                controller: locationController,
                              )
                            else
                              const Text('Universidad de los andes'),
                            const SizedBox(
                              height: 20,
                            ),
                            if (_isDriver ?? true)
                              Column(
                                children: [
                                  const Text(
                                    'Precio por cupo:',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: Colors.black54),
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  TextFormField(

                                    decoration: const InputDecoration(
                                      border: OutlineInputBorder(),
                                      hintText: 'Ingrese precio deseado',
                                    ),
                                    controller: priceController,
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(
                                          RegExp('[0-9]'))
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  const Text(
                                    'Numero de pasajeros:',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: Colors.black54),
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  TextFormField(

                                    decoration: const InputDecoration(
                                      border: OutlineInputBorder(),
                                      hintText: 'Ingrese cupos disponibles',
                                    ),
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(
                                          RegExp('[0-9]'))
                                    ],
                                  ),
                                ],
                              )
                            else
                              Column(
                                children: [
                                  const Text(
                                    'Precio maximo:',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: Colors.black54),
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  TextFormField(

                                    decoration: const InputDecoration(
                                      border: OutlineInputBorder(),
                                      hintText: 'Ingrese precio maximo deseado',
                                    ),
                                    keyboardType: TextInputType.number,
                                    controller: priceController,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(
                                          RegExp('[0-9]'))
                                    ],
                                  ),
                                ],
                              ),
                            const SizedBox(
                              height: 30,
                            ),
                            SizedBox(
                              width: 170, // <-- Your width
                              height: 50, // <-- Your height
                              child: ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          const Color(0xFF00468B)),
                                ),
                                onPressed: () async {
                                  if (locationController.text == "") {
                                    if (_goingHome ?? true) {
                                      Fluttertoast.showToast(
                                          msg: "Por favor seleccionar destino");
                                    } else {
                                      Fluttertoast.showToast(
                                          msg:
                                              "Por favor seleccionar punto de inicio");
                                    }
                                  } else if (priceController.text == "") {
                                    Fluttertoast.showToast(
                                        msg:
                                            "Por favor seleccionar precio maximo a pagar");
                                  } else {

                                    if (_goingHome ?? true) {
                                      start = "Universidad de los andes";
                                      end = locationController.text;
                                    } else {
                                      start = locationController.text;
                                      end = "Universidad de los andes";
                                    }
                                  context.read<TripListBloc>().add(TripPreferencesSaved(_goingHome, priceController.text, start, end));
                                    if(context.read<TripBloc>().state.isDriver) {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>  HomePageMap()));
                                    } else {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) => const TravelList()));
                                    }
                                    
                                  }
                                },
                                child: const Text("Buscar"),
                              ),
                            )
                          ],
                        )));
              })),
    ));
  }
}
