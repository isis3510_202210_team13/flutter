import 'package:wheelio_app/trip_search/models/trip.dart';

import '../services/trip_list_service.dart';

abstract class TripListRepository {

  Future getTripList();
  Future<List<Trip>> getTripListLocalStorage();
  Future<void> updateTripListLocalStorage(List<Trip> pTripList);


  Future<void> updateTripPreferencesLocalStorage(List<String> preferences);
  Future<List<String>?> getTripPreferencesLocalStorage();
}

class TripListRepositoryImpl implements TripListRepository {
  TripListService service = TripListService();

  @override
  Future getTripList() {
    return service.getTripList();
  }

  @override
  Future<List<Trip>> getTripListLocalStorage()  {
    return  service.getTripListLocalStorage();
  }

  @override
  Future<void> updateTripListLocalStorage(List<Trip> pTripList) async {
    return await service.updateTripListLocalStorage(pTripList);
  }

  @override
  Future<List<String>?> getTripPreferencesLocalStorage() {
    return service.getTripPreferencesLocalStorage();
  }

  @override
  Future<void> updateTripPreferencesLocalStorage(List<String> preferences) {
    return service.updateTripPreferencesLocalStorage(preferences);
  }
}
