import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wheelio_app/api/search_api.dart';
import 'package:wheelio_app/models/places.dart';
import 'package:wheelio_app/utils/debounce.dart';

class OriginalAndDestinationPage extends StatefulWidget {
  final Place departure, arrival;
  final List<Place> history;
  final void Function(Place origin) onOriginChanged;

  OriginalAndDestinationPage({
    Key? key,
    required this.departure,
    required this.history,
    required this.onOriginChanged,
    required this.arrival,
  }) : super(key: key);

  @override
  State<OriginalAndDestinationPage> createState() =>
      _OriginalAndDestinationPageState();
}

class _OriginalAndDestinationPageState
    extends State<OriginalAndDestinationPage> {
  final FocusNode _originFocusNode = FocusNode();
  final FocusNode _destinationFocusNode = FocusNode();

  bool _originHasFocus = false;
  bool _searching = false;
  List<Place> _results = [];
  ValueNotifier<String> _query = ValueNotifier('');

  late TextEditingController _originController, _destinationController;

  final SearchAPI _searchAPI = SearchAPI.instance;
  final Debounce _debounce =
      Debounce(duration: const Duration(milliseconds: 300));

  StreamSubscription? _streamSubscription;

  @override
  void dispose() {
    _originFocusNode.dispose();
    _destinationFocusNode.dispose();
    _originController.dispose();
    _destinationController.dispose();
    _searchAPI.cancel();
    _debounce.cancel();
    _streamSubscription?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _originController = TextEditingController(text: widget.departure.title);

    if (widget.arrival.title != '') {
      _destinationController =
          TextEditingController(text: widget.arrival.title);
    } else {
      _destinationController = TextEditingController();
    }

    _originFocusNode.addListener(() {
      setState(() {
        _originHasFocus = true;
      });
    });
    _destinationFocusNode.addListener(() {
      setState(() {
        _originHasFocus = false;
      });
    });

    _destinationFocusNode.requestFocus();
  }

  void _onInputChange(String query) {
    _query.value = query;
    _streamSubscription?.cancel();
    _searchAPI.cancel();
    if (query.trim().length > 3) {
      setState(() {
        _searching = true;
      });
      _debounce.create(() => _searchCall(query));
    } else {
      if (_searching || _results.isNotEmpty) {
        setState(() {
          _searching = false;
          _results = [];
        });
      }
      _debounce.cancel();
    }
  }

  void _searchCall(String query) {
    _streamSubscription = _searchAPI
        .searchPlace(query, widget.departure.position)
        .asStream()
        .listen((List<Place> results) {
      setState(() {
        _searching = false;
        _results = results;
      });
    });
  }

  Widget _buildList(bool isHistory) {
    List<Place> items = isHistory ? widget.history : _results;

    if (isHistory) {
      items = items
          .where(
            (element) =>
                element.title.toLowerCase().contains(_query.value) ||
                element.vicinity.toLowerCase().contains(_query.value),
          )
          .toList();
    }

    return ListView.builder(
      itemBuilder: (_, index) {
        final Place item = items[index];
        return ListTile(
          onTap: () {
            if (_originHasFocus) {
              _originController.text = item.title;
              widget.onOriginChanged(item);
              _destinationFocusNode.requestFocus();
            } else {
              Navigator.pop(context, item);
            }
          },
          leading: isHistory ? const Icon(Icons.history) : null,
          title: Text(
            item.title,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(item.vicinity.replaceAll('<br/>', '-')),
        );
      },
      itemCount: items.length,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        title: const Text(
          '¿A dónde vas?',
          style: TextStyle(color: Colors.black),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          color: Colors.black,
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    _TextInputWidget(
                      icon: Icons.gps_fixed,
                      placeHolder: 'Donde estás',
                      focusNode: _originFocusNode,
                      hasFocus: _originHasFocus,
                      controller: _originController,
                      onChanged: _onInputChange,
                    ),
                    SizedBox(height: 10),
                    _TextInputWidget(
                      icon: Icons.not_listed_location_sharp,
                      placeHolder: 'A donde vas',
                      focusNode: _destinationFocusNode,
                      hasFocus: !_originHasFocus,
                      controller: _destinationController,
                      onChanged: _onInputChange,
                    ),
                  ],
                ),
              ),
              if (_searching)
                const Expanded(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              else
                Expanded(
                  child: ValueListenableBuilder<String>(
                    valueListenable: _query,
                    builder: (_, query, __) =>
                        _buildList(query.trim().length <= 3),
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}

class _TextInputWidget extends StatelessWidget {
  final IconData icon;
  final String placeHolder;
  final FocusNode focusNode;
  final bool hasFocus;
  final TextEditingController controller;
  final void Function(String) onChanged;

  const _TextInputWidget({
    Key? key,
    required this.icon,
    this.placeHolder = '',
    required this.focusNode,
    this.hasFocus = false,
    required this.controller,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(icon),
        Flexible(
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 10),
            padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                20,
              ),
              color: Colors.black12,
              border: Border.all(
                color: hasFocus ? Colors.blue : Colors.black12,
              ),
            ),
            child: TextField(
              controller: controller,
              focusNode: focusNode,
              onChanged: onChanged,
              decoration: InputDecoration.collapsed(hintText: placeHolder),
            ),
          ),
        ),
      ],
    );
  }
}
