import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wheelio_app/Authentication/bloc/authentication_bloc.dart';
import 'package:wheelio_app/Authentication/repository/authentication_repository.dart';

import 'package:wheelio_app/Authentication/ui/screens/home_login.dart';
import 'package:wheelio_app/Connectivity/bloc/connectivity_bloc.dart';
import 'package:wheelio_app/Connectivity/repository/connectivity_repository.dart';
import 'package:wheelio_app/Database/bloc/database_bloc.dart';
import 'package:wheelio_app/Database/repository/database_repository.dart';
import 'package:wheelio_app/trip_active/bloc/trip_bloc.dart';
import 'package:wheelio_app/trip_active/repository/trip_repository.dart';
import 'package:wheelio_app/trip_search/bloc/trip_list_bloc.dart';
import 'package:wheelio_app/trip_search/repository/trip_list_repository.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  await Firebase.initializeApp();
  BlocOverrides.runZoned(
    () => runApp(MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              AuthenticationBloc(AuthenticationRepositoryImpl()),
        ),
        BlocProvider(
          create: (context) =>
              DatabaseBloc(DatabaseRepositoryImpl())
                ..add(DatabaseStarted()),
        ),
        BlocProvider(
          create: (context) =>
              ConnectivityBloc(ConnectivityRepositoryImpl())
              ..add(const ConnectivityStarted()),
        ),
        BlocProvider(
          create: (context) =>
              TripListBloc(TripListRepositoryImpl()),
        ),
                BlocProvider(
          create: (context) =>
              TripBloc(initialTrip: null, tripRepository: TripRepositoryImpl()),
        ),
      ],
      child: MultiRepositoryProvider(
  providers: [
    RepositoryProvider<ConnectivityRepositoryImpl>(
      create: (context) => ConnectivityRepositoryImpl(),
    ),
    RepositoryProvider<DatabaseRepositoryImpl>(
      create: (context) => DatabaseRepositoryImpl(),
    ),
  ],
  child: const MyApp(),
)
    )),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
        title: 'Wheelio',
        theme: ThemeData(),
        home: const HomeLogin(),
        );
  }
}

