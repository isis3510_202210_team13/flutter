import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:wheelio_app/Connectivity/services/connectivity_service.dart';

abstract class ConnectivityRepository {
  Future<void> initConnectivity();
  Future<void> showBasicsFlash(BuildContext context);
  Future<void> showConnectionRestablishedFlash(BuildContext context);
}

class ConnectivityRepositoryImpl implements ConnectivityRepository {
  ConnectivityService service = ConnectivityService();

  @override
  Future<void> initConnectivity() async {
    return service.initConnectivity();
  }

  @override
  Future<void> showBasicsFlash(BuildContext context) {
    return service.showBasicsFlash(context: context);
  }
@override
Future<void> showConnectionRestablishedFlash(BuildContext context) {
   return service.showConnectionRestablishedFlash(context: context);
}
}