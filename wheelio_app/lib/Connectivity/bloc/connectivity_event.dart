part of 'connectivity_bloc.dart';

abstract class ConnectivityEvent extends Equatable {
  const ConnectivityEvent();

  @override
  List<Object> get props => [];
}

class ConnectivityStarted extends ConnectivityEvent {

  const ConnectivityStarted();

    @override
  List<ConnectivityResult> get props => [];
}

class ConnectivityChanged extends ConnectivityEvent {
  const ConnectivityChanged();
}