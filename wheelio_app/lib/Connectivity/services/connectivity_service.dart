import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ConnectivityService {
  //eventual connectivity
  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();

  Future<void> initConnectivity() async {
    late ConnectivityResult result;

    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      return;
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result;
  }

  Future<void> showBasicsFlash({
    Duration? duration,
    flashStyle = FlashBehavior.floating,
    required BuildContext context,
  }) {
    return showFlash(
        context: context,
        duration: const Duration(seconds: 15),
        builder: (context, controller) {
          return Flash(
            enableVerticalDrag: false,
            backgroundColor: const Color(0xFF00468B),
            controller: controller,
            behavior: FlashBehavior.fixed,
            position: FlashPosition.bottom,
            boxShadows: kElevationToShadow[4],
            child: FlashBar(
              icon: const Icon(Icons.signal_wifi_off_rounded),
              content: const Text('No hay conexión a internet!', style: TextStyle(color: Colors.white)),
            ),
          );
        });
  }

    Future<void> showConnectionRestablishedFlash({
    Duration? duration,
    flashStyle = FlashBehavior.floating,
    required BuildContext context,
  }) {
    return showFlash(
        context: context,
        duration: const Duration(seconds: 15),
        builder: (context, controller) {
          return Flash(
            enableVerticalDrag: false,
            backgroundColor: const Color(0xFF00468B),
            controller: controller,
            behavior: FlashBehavior.fixed,
            position: FlashPosition.bottom,
            boxShadows: kElevationToShadow[4],
            child: FlashBar(
              icon: const Icon(Icons.signal_cellular_alt_rounded),
              content: const Text('Se reestableció la conexión a internet!', style: TextStyle(color: Colors.white)),
            ),
          );
        });
  }
}
